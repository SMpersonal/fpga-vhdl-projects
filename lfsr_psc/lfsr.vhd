library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lfsr is
    port(
        clk : in std_logic;
        lfsr_output : out std_logic;
        update_indicator : out std_logic
    );
end entity;

architecture arch_lfsr of lfsr is 
    signal new_clk : std_logic := '0';
    signal seed : std_logic_vector(7 downto 0) := x"A6";
    signal set_state : std_logic := '1';
    signal update_ind : std_logic := '0';
    signal lfsr_state : std_logic_vector(7 downto 0);
    signal lfsr_out : std_logic;
begin
    lfsr_output <= lfsr_out;
    update_indicator <= update_ind;
    lfsr_out <= lfsr_state(0) xor lfsr_state(3);
    process(new_clk) is
    begin
        if rising_edge(new_clk) then
            if set_state = '1' then
                lfsr_state <= seed;
                set_state <= not set_state;
            else
                update_ind <= not update_ind;
                lfsr_state(7) <= (lfsr_state(0) xor lfsr_state(2)) xor lfsr_state(6);
                lfsr_state(6 downto 0) <= lfsr_state(7 downto 1);
            end if;
        end if;
    end process;

    process(clk) is
        variable i : integer := 0;
    begin
        if rising_edge(clk) then
            if i < 25000000 then
                new_clk <= '1';
                i := i + 1;
            elsif i >= 25000000 and i < 50000000 then
                new_clk <= '0';
                i := i + 1;
            else 
                i := 0;
            end if;
        end if;
    end process;
end architecture;
