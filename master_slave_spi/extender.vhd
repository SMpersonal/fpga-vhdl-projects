library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
entity extender is
	port (DOUT: out std_logic := '0';	
			DIN : in std_logic;
			 CS: in std_logic;		
				SCLK: in std_logic;
       P : out unsigned (7 downto 0) := x"00");		
end extender;

architecture arch_extender of extender is
component spi_receiver is
	port(
		SCLK : in std_logic;
		CS : in std_logic;
		DIN : in std_logic;
		DOUT : out std_logic := '0';
		ri : in std_logic;
		rc : out std_logic := '0';
		wi : in std_logic;
		wc : out std_logic := '0';
		wdata : in unsigned (7 downto 0);
		rdata : out unsigned (7 downto 0) := x"00"
	);
end component;
	signal ri : std_logic := '0';
	signal rc : std_logic;
	signal wi : std_logic := '0';
	signal wc : std_logic;
	signal wdata : unsigned (7 downto 0) := x"00";
	signal rdata : unsigned (7 downto 0);
	signal io_register : unsigned (7 downto 0) := x"56";
	signal pomoc : std_logic := '0';
begin
	inst : spi_receiver port map (SCLK => SCLK, CS => CS, DIN => DIN, DOUT => DOUT, ri => ri, rc => rc, wi => wi, wc => wc, wdata => wdata, rdata => rdata);
process(rc, wc, CS)
variable state : integer := 0;
begin
	if(CS'event and CS = '0') then
		if (state = 0) then
			ri <= '1';
			state := 1;
		elsif (state = 1) then
			if(rdata = x"1A") then
				wi <= '1';
				wdata <= io_register;
				state := 0;
			elsif(rdata = x"2B") then
				ri <= '1';
				state := 2;
		  end if;
		end if;
	elsif(rising_edge(CS)) then
		ri <= '0';
		wi <= '0';
	end if;
	if(rising_edge(rc)) then
		if(state >= 2) then
			io_register <= rdata;
			state := 0;
		end if;
	end if;
end process;
P <= io_register;
end arch_extender;