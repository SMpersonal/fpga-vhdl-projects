library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity simulation is
end simulation;

architecture behavior of simulation is

component top_extender is
	port (
        fclk : in std_logic;
        cmd : in unsigned (7 downto 0);
        wdata : in unsigned (7 downto 0);
        rdata : out unsigned (7 downto 0) := x"00";
        bflag : in std_logic;
        eflag : out std_logic := '0');
end component;

signal iclk : std_logic := '0';
signal cmd : unsigned(7 downto 0) := x"00";
signal wdata : unsigned(7 downto 0) := x"00";
signal rdata : unsigned(7 downto 0);
signal bflag : std_logic := '0';
signal eflag : std_logic;
begin
uut : top_extender port map(
    fclk=>iclk,
    cmd => cmd,
    wdata => wdata,
    rdata => rdata,
    eflag => eflag,
    bflag => bflag
    );
  process
  constant fclk_period: time := 20 ns;
  begin
      iclk <= '0';
      wait for fclk_period/2;
      iclk <= '1';
      wait for fclk_period/2;
  end process;
  process
  constant fclk_period: time := 20 ns;
  begin
    cmd <= x"1A";
    bflag <= '1';
    wait for 200 * fclk_period;
    bflag <= '0';
    wait for 1420 * fclk_period;
    cmd <= x"2B";
    wdata <= x"A5";
    bflag <= '1';
    wait for 200 * fclk_period;
    bflag <= '0';
    wait for 1420 * fclk_period;
  end process;
end behavior;