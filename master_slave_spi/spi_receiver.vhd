library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity spi_receiver is
	port(
		SCLK : in std_logic;
		CS : in std_logic;
		DIN : in std_logic;
		DOUT : out std_logic := '0';
		ri : in std_logic;
		rc : out std_logic := '0';
		wi : in std_logic;
		wc : out std_logic := '0';
		wdata : in unsigned (7 downto 0);
		rdata : out unsigned (7 downto 0) := x"00"
	);
end spi_receiver;

architecture a_spi_receiver of spi_receiver is
signal rxdata: unsigned (7 downto 0) := x"00";
begin
	read: process(SCLK, CS, ri)
	variable idx : integer := 8;
	variable rflag : integer := 0;
	begin
		if(rising_edge(ri)) then
			rflag := 1;
			idx := 8;
			rc <= '0';
		end if;
		if(rising_edge(SCLK)) then
			if(rflag = 1) then
				idx := idx - 1;
				rxdata(idx) <= DIN;
				if(idx = 0) then
					rc <= '1';
					rflag := 0;
				end if;
			end if;
		end if;
	end process;	
	write: process(SCLK, wi, CS)
	variable idx : integer := 8;
	variable txflag : integer := 0;
	begin
		if(rising_edge(wi)) then
			txflag := 1;
			wc <= '0';
			idx := 7;
			DOUT <= wdata(idx);
		end if;
		if(falling_edge(SCLK)) then
			if(txflag = 1) then
				idx := idx - 1;
				DOUT <= wdata(idx);
				if(idx = 0) then
					wc <= '1';
					txflag := 0;
				end if;
			else
				DOUT <= '0';
			end if;
		end if;
	end process;	
	rdata <= rxdata;
end a_spi_receiver;

