library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
entity top_extender is
	port (
        fclk : in std_logic;
        cmd : in unsigned (7 downto 0);
        wdata : in unsigned (7 downto 0);
        rdata : out unsigned (7 downto 0) := x"00";
        bflag : in std_logic;
        eflag : out std_logic := '0');
end top_extender;

architecture arch_extender of top_extender is
	signal rc : std_logic;
	signal ri : std_logic := '0';
	signal wi : std_logic := '0';
	signal wc : std_logic;
	signal wdata_temp : unsigned (7 downto 0) := x"00";
	signal ioregister : unsigned (7 downto 0);
	signal sclk_temp : std_logic;
	signal cs_temp : std_logic;
	signal mosi_temp : std_logic;
	signal miso : std_logic := '0';

component spi_profesor is
  port( iclk: in std_logic;
	mosi: out std_logic:='L';
	miso: in std_logic;
	sck: out std_logic:='L';
	cs: out std_logic:='H';
	wdata: in unsigned(7 downto 0);
	wi: in std_logic;
	wc: out std_logic:='0';
	rdata: out unsigned (7 downto 0) := x"00";
	ri: in std_logic;
	rc: out std_logic:='0');
end component;
component extender is
	port (DOUT: out std_logic := '0';	
			DIN : in std_logic;
			 CS: in std_logic;		
				SCLK: in std_logic;
       P : out unsigned (7 downto 0) := x"00");		
end component;
begin
	inst : spi_profesor port map (iclk => fclk, sck => sclk_temp, cs => cs_temp, mosi => mosi_temp, miso => miso, rc => rc,ri => ri, wi => wi, wc => wc, wdata => wdata_temp, rdata => rdata);
	ekstender : extender port map (SCLK => sclk_temp, CS => cs_temp, P => ioregister, DIN => mosi_temp, DOUT => miso);

	
process(fclk)
variable cnt : integer := 0;
variable state : integer := 0;
variable start : integer := 0;
variable write : integer := 0;
begin
	if(fclk'event and fclk = '1') then
		if(bflag = '1' and start = 0) then
			wi <= '1';
			wdata_temp <= cmd;
			start := 1;
		end if;
		if(start = 1) then
			cnt := cnt + 1;
			if(state = 0) then
				if(cnt = 200) then
					wi <= '0';
				end if;
				if(cnt >= 410 and wc = '1') then
					cnt := 0;
					state := 1;
					if(cmd = x"2B") then
						write := 1;
					end if;
				end if;
			elsif(state = 1) then
				if(cnt = 500) then
					state := 2;
					cnt := 0;
				end if;
			elsif(state = 2) then
				if(write = 1) then
					wi <= '1';
					wdata_temp <= wdata;
				else
					ri <= '1';
				end if;
				state := 3;
				cnt := 0;
			elsif(state = 3) then
				if(cnt = 200) then
					ri <= '0';
					wi <= '0';
				end if;
				if(cnt >= 410 and (wc = '1' or rc = '1')) then
					eflag <= '1';	
					state := 4;
					cnt := 0;
				end if;
			else 
				if (cnt = 200) then
					state := 0;
					start := 0;
					cnt := 0;
					write := 0;
					eflag <= '0';
				end if;
			end if;
	end if;
end if;
end process;
end arch_extender;