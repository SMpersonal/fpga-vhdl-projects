handler = open("notes.txt","r")
handler_write = open("vhdl_code.txt","w")
base_freq = 220
fpga_freq = 100 * 10**6
a = 2 ** (1/12.0)
period_hash = dict([("A",base_freq),("B",int(base_freq * a**2)),("C",int(base_freq * a**3)),("D",int(base_freq * a**5)),("E",int(base_freq * a**7)),("F",int(base_freq * a**8)),("G",int(base_freq * a**11))])
wait_hash = dict([("o",int(fpga_freq/8.0)),("c",int(fpga_freq/4.0)),("p",int(fpga_freq/2.0))])
led_hash = dict([("A",1),("B",1 << 1),("C",1 << 2),("D",1 << 3),("E",1 << 4),("F",1 << 5),("G",1 << 6)])
duty_inp = list()
period_inp = list()
time_inp = list()
led_inp = list()
for line in handler:
  line = line.rstrip()
  period = int(fpga_freq / period_hash[line[0]])
  period_inp.append(period)
  duty_inp.append(int(period / 2))
  time_inp.append(wait_hash[line[1]])
  led_inp.append(led_hash[line[0]])
handler_write.write("time array\n")
for cnt in range(len(time_inp)):
  handler_write.write(f'{cnt} => "{format(time_inp[cnt],"032b")}",\n')
handler_write.write("duty array\n")
for cnt in range(len(duty_inp)):
  handler_write.write(f'{cnt} => "{format(duty_inp[cnt],"032b")}",\n')
handler_write.write("period array\n")
for cnt in range(len(period_inp)):
  handler_write.write(f'{cnt} => "{format(period_inp[cnt],"032b")}",\n')
handler_write.write("led array\n")
for cnt in range(len(led_inp)):
  handler_write.write(f'{cnt} => "{format(led_inp[cnt],"08b")}",\n')